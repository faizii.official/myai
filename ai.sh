#!/bin/bash

# Check if jq is installed
if ! command -v jq &> /dev/null
then
    echo "jq not found. Attempting to install..."
    # Try to install jq using the appropriate package manager for your system
    if command -v apt-get &> /dev/null
    then
        sudo apt-get update && sudo apt-get install -y jq dialog
    elif command -v yum &> /dev/null
    then
        sudo yum install -y jq dialog
    elif command -v pacman &> /dev/null
    then
        sudo pacman -S --noconfirm jq dialog
    elif command -v apk &> /dev/null
    then
        apk add jq dialog
    elif command -v brew &> /dev/null
    then
        brew install  jq dialog
    else
        echo "Unable to install jq automatically. Please install jq manually."
        exit 1
    fi
fi

echo "jq is now installed."

rm ai > /dev/null 2>&1
clear
i=$(dialog --title "This AI API is created by faizan quazi" --inputbox "Enter your question:" 0 0 3>&1 1>&2 2>&3)

export i=$i
clear
dialog --title "Faizii's API v1 Thinking." --infobox "Question is $i?" 0 0
curl -s https://gitlab.com/faizii.official/myai/-/raw/main/help.txt |sed "s/changeme/${i}/g" |bash

